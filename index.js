const express = require("express")
const mysql = require('mysql2');
const bodyParser = require('body-parser');
const cors = require('cors');

// // MySQL Connection
const pool = mysql.createPool({
    host: 'mysql-tbsn.alwaysdata.net',
    user: 'tbsn',
    password: 'alwaysthom_45',
    database: 'tbsn_languages',
    waitForConnections: true,
});

const api = express()

// Middleware
api.use(bodyParser.urlencoded({ extended: true }));
api.use(bodyParser.json());
api.use(cors({
    origin: "*"
}))

// Create a new language
api.post('/new/language', (req, res) => {
    const { name } = req.query.name;
    const query = `INSERT INTO languages (language) VALUES ${name}`;
    pool.query(query, (err, results) => {
        if (err) {
            console.error(err);
            return res.status(500).json({ error: 'Internal Server Error' });
        }
        return res.status(200).json(results);
    });
});

api.get('/', async (req, res) => {
    return res.status(200).json("coucou");
});

// Get all languages
api.get('/languages', async (req, res) => {
    pool.query(`SELECT * FROM languages`, (err, results) => {
        if (err) {
            console.error(err);
            return res.status(500).json({ error: 'Internal Server Error' });
        }
        return res.status(200).json(results);
    });
});

// Create a new word
api.post('/new/words', (req, res) => {
    const { languageId, baseWord, translatedWord } = req.query;
    const query = `INSERT INTO translations (languageId, translatedWord, baseWord) VALUES (${languageId}, ${translatedWord}, ${baseWord})`;
    pool.query(query, (err, results) => {
        if (err) {
            console.error(err);
            return res.status(500).json({ error: 'Internal Server Error' });
        }
        return res.status(200).json(results);
    });
});

// Get all words
api.get('/words', async (req, res) => {
    const {baseLanguageId, translationLanguageId, limit} = req.query;
    pool.query(`SELECT translatedWord, baseWord FROM translations WHERE baseLanguageId = ? AND translationLanguageId = ? ORDER BY RAND() LIMIT ?`, [baseLanguageId, translationLanguageId, parseInt(limit)], (err, results) => {
        if (err) {
            console.error(err);
            return res.status(500).json({ error: err , body: req.body, query: req.query});
        }
        return res.status(200).json(results);
    });
});
const port = process.env.PORT || 3000

api.listen(port, () => console.log(`Listening to port ${port}`));